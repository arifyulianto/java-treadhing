/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Belajar;

/**
 *
 * @author VOLD
 */
public class Threading implements Runnable {
    int angka;
    String namaThread;
    
    public Threading(String nama){
        angka = 5;
        namaThread = nama;
    }
    
    public void run(){
        try {
            do {
                Thread.sleep(1000);
                System.out.println(namaThread+", Menghitung "+angka);
                angka--;
            } while (angka>0);
            System.out.println(namaThread+" SELESAI MENGHITUNG");
        } catch (InterruptedException e) {
            System.out.println("error!");
        }
    }

}
